== Contributing

. If you want to add feature or change behavior, first start a new topic in our https://wovalab-open-source-projects.zulipchat.com/#[Zulip chat room] and then create an issue so we can discuss about it.
. Fork the repo.
. Make a pull request with your changes.

NOTE: A merge request can be hard to handle if it modify too much files. That's why we encourage you to start by discussing the modifications you want to share before coding them. We would be sad not to be able merge them.

TIP: More info about Forking workflow with Git https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow[here].

IMPORTANT: Code contribution *MUST* follow the https://dqmh.org/content/styleguide[DQMH Consortium Style Guide]

== Development requirements

* LabVIEW 2014 SP1 32 bit

== Dependencies

=== Mandatory
* https://www.vipm.io/package/wovalab_lib_antidoc/[Antidoc] and any suitable document type add-ons
* https://www.vipm.io/package/wovalab_lib_asciidoc_for_labview/[Asciidoc for LabVIEW]
* https://www.vipm.io/package/wiresmith_technology_lib_g_cli/[G CLI]